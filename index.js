// By convention, class names should begin with an uppercase character
class Student{
    // the constructor method defines how objects created from this class will be assigned their initial property values 
    constructor(name, email, grades){
        this.name = name
        this.email = email
        this.gradeAve = undefined
        this.pass = undefined
        this.honor = undefined

        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades
            } else{
                this.grades = undefined
            }
        } else{
            this.grades = undefined
        }
    }
    login(){
        console.log(`${this.email} has logged in`)
        return this
    }
    logout(){
        console.log(`${this.email} has logged out`)
        return this
    }
    listGrades(){
        console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
        console.log(this.grades)
        return this
    }
    gradeAverage(){
        let sum = 0;
        for(let i = 0; i < this.grades.length; i++ ){
        sum += parseInt( this.grades[i], 10 );
        }

        let avg = sum/this.grades.length;

        this.gradeAve = avg
        console.log(this.gradeAve)
        return this
    }
    willPass(){
        if(this.gradeAverage().gradeAve >= 85){
            this.pass = true
        }
        else{
            this.pass = false
        }
        console.log(this.pass)
        return this
    }
    willPassWithHonors(){
        if(this.gradeAverage().gradeAve >= 90){
            this.honor = true
        } else if(this.gradeAverage().gradeAve >= 85 && this.gradeAverage().gradeAve < 90){
            this.honor = false
        } else{
            this.honor = undefined
        }
        console.log(this.honor)
        return this
    }
}

// instatiate / create objects from our Student class
let studentOne = new Student("John", "john@mail.com", [89, 84, 85, 88])
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85])
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93])
let studentFour = new Student("Jessie", "joe@mail.com", [91, 89, 92, 93])

console.log(studentOne.listGrades())
console.log(studentTwo.gradeAverage())
console.log(studentThree.willPass())
console.log(studentFour.willPassWithHonors())